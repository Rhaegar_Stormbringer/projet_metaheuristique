package main;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

import java.util.Random;

import tools.Tools;
import tools.neighborsEnum;
import verifier.EvacuationGraph;
import verifier.EvacuationRoad;
import verifier.EvacuationTree;
import verifier.Solution;
import verifier.SolutionMemory;
import verifier.SourceSegment;

@SuppressWarnings("unused")
public class Main {

	public static void main(String [] args) {
		
//******************************************************************//
// * Paramètres de fonctionnement par defaut *                      //
//******************************************************************//
		LocalDateTime t0      = LocalDateTime.now();
		int lgExploPlateau    = 1;
		int nbDiversification = 7;
		int limiteVoisins     = 10000;
		int degDivO           = -1;
		//int degDivF           = -1;
		int randomStepProba   = 15;
		
		boolean verbose       = true;
		
		String inFileName     = "sparse_10_30_3_4_I.full";
		String outFileRad     = "Sol_BN";
		
//******************************************************************//
// * Init *                                                         //
//******************************************************************//
		
		int cptI = lgExploPlateau;
		int cptD = nbDiversification;
		
		int oldScore = -1;
		int newScore = -1;
		
		int rsp = randomStepProba -1;
		
		Solution ws;
		Random r = new Random();
		
		int it = 0;
		
//******************************************************************//
// * Chargement de l'arbre d'évacuation *                           //
//******************************************************************//
		if (verbose) {
			System.out.println("Chargement du fichier source ...");
		}
		
		EvacuationTree eT = new EvacuationTree(inFileName);
		
		if (verbose) {
			System.out.println("[fait]\n");
		}
		
//******************************************************************//
// * Bornes *                                                       //
//******************************************************************//
				
		System.out.println(Tools.borneInf(eT, "inf.sol"));
		System.out.println(Tools.borneSup(eT, "sup.sol",""));
		
		
		
		
//******************************************************************//
// * Création d'une solution *                                      //
//******************************************************************//
		
		if (verbose) {
			System.out.println("Création d'une solution initiale ...");
		}
		
		int[] e = new int[eT.getSources().size()];
		for (int i=0; i < e.length; i++) {
			e[i] = eT.getSources().get(i).getFrom();
		}		
		Solution s  = new Solution(e);
		Solution ps = s;
		Solution bs = s;
		
		if (verbose) {
			System.out.println("[fait]\n");
		}
		
//******************************************************************//
// * Initialisation d'une solution *                                //
//******************************************************************//
		
		if (verbose) {
			System.out.println("Initialisation de la solution ...");
		} 
		
		Solution.init(s, eT);
		Solution.place(s, eT);
		newScore = s.getValue();
		
		if (verbose) {
			System.out.println("| Score : "+newScore);
			System.out.print("| Ecriture ... ");
		}
		Solution.writeSolution(s, eT, outFileRad+"_"+it, "sol");
		
		if (verbose) {
			System.out.println("ok");
			System.out.println("[fait]\n");
		}
		
//******************************************************************//
// * Création d'une memoire *                                       //
//******************************************************************//
		
		if (verbose) {
			System.out.println("Création d'une mémoire de solutions ...");
		}
		
		SolutionMemory sm = new SolutionMemory(s);
		sm.addToExplored(s);		
		
		if (verbose) {
			System.out.println("[fait]\n");
		}
		
//******************************************************************//
// * Intensification & Diversification *                            //
//******************************************************************//
		
		while (((cptI > 0)||(cptD > 0))&&(sm.sizeKnown() < limiteVoisins)) {
			it++;
			if (cptI != 0) { // * INTENSIFICATION * //
				
//** CREATION DUN VOISINAGE ****************************************//
				
				if (verbose) {
					System.out.println("[i] Recherche de voisins <"+(it-1)+"-"+(it)+"> ...");
					System.out.print("| Création voisinage ... ");
				}
				
				ArrayList<Solution> p = s.neighbors(neighborsEnum.ORDER);
				
				if (verbose) {
					System.out.println("ok");								
					System.out.print("| Vérification mémoire ... ");
				}
				
				p = sm.removeMemorizedSolutionsFrom(p);
				
				if (verbose) {			
					System.out.println("ok");
					System.out.println("| Voisins découverts : " + p.size());
				}
				
				if (!p.isEmpty()) {
					
//** HILL-CLIMB vs RANDOM STEP *************************************//
						if (r.nextInt(100) < rsp) {
	
//** RANDOM STEP ***************************************************//
							
							if (verbose) {
								System.out.println("[stop]\n");
								System.out.println("[r] Selection d'un voisin aléatoire <"+(it)+">");
							}
							
							ps = s;
							s = p.get(r.nextInt(p.size()));
							sm.addToExplored(s);
							
							if (verbose) {
								System.out.print("| Construction du voisins ... ");
							}
							
							Solution.init(s, eT);
							Solution.place(s, eT);
							//Solution.adjust(s, eT);
														
							if (verbose) {
								System.out.println("ok");
								System.out.print("| Mise a jour mémoire ... ");
							}
							
							oldScore = ps.getValue();
							newScore = s.getValue();
							
							if (verbose) {
								System.out.println("ok");
								System.out.println("| Ancien score  : "+oldScore);
								System.out.println("| Nouveau score : "+newScore);
								System.out.print("| Mise à jour mémoire ... ");
							}
								
							sm.addToExplored(s);
								
							if (verbose) {
								System.out.println("ok");
								System.out.print("| Ecriture ... ");
							}
								
							ws = sm.revertToOriginalOrder(s);
							Solution.writeSolution(ws, eT, outFileRad+"_"+(it), "sol");
								
							if (verbose) {
								System.out.println("ok");
								System.out.println("[fait]\n");
							}
								
							if (oldScore == newScore) {
								cptI--;
							}
							
						} else { 

//** HILL-CLIMBING ************************************************//
							
							if (verbose) {
								System.out.print("| Construction des voisins ... ");
							}
							
							Solution.init(p, eT);
							Solution.place(p, eT);
							//Solution.adjust(p, eT);
							
							if (verbose) {
								System.out.println("ok");
								System.out.print("| Mise a jour mémoire ... ");
							}
							
							sm.addToKnown(p);
							
							if (verbose) {
								System.out.println("ok");
								System.out.println("[fait]\n");
							}
				
							if (verbose) {
								System.out.println("[i] Selection du meilleur voisin <"+(it)+">");
							}
						
							ps = s;
							s = Collections.min(p);
							sm.addToExplored(s);
					
							oldScore = ps.getValue();
							newScore = s.getValue();
							
							if (oldScore >= newScore) {
															
								if (verbose) {
									System.out.println("| Ancien score  : "+oldScore);
									System.out.println("| Nouveau score : "+newScore);
									System.out.print("| Mise à jour mémoire ... ");
								}
									
								if (verbose) {
									System.out.println("ok");
									System.out.print("| Ecriture ... ");
								}
									
								ws = sm.revertToOriginalOrder(s);
								Solution.writeSolution(ws, eT, outFileRad+"_"+it, "sol");
									
								if (verbose) {
									System.out.println("ok");
									System.out.println("[fait]\n");
								}
									
								if (oldScore == newScore) {
									cptI--;
								} else {
									cptI = lgExploPlateau;
								}
							} else {
								System.out.println("| Absence de meilleure solution : minimum local atteint");
								System.out.println("[fait]\n");
								cptI = 0;
							}
						}
						
//** ABSENCE DE VOISIN ************************************************//		
						
					} else {
						System.out.println("| Absence de nouveaux voisins : minimum local atteint");
						
						System.out.println("[fait]\n");
						cptI = 0;
					}
				
//** DIVERSIFICATION PAR PERTURBATION **********************************//	
				
			} else { 
				
				if (verbose) {
					System.out.println("[d] Diversification ...");
					System.out.print("| Modification de l'ordre ... ");
				}
				
				s = Solution.shuffle(bs, degDivO);
				ps = s;
				
				if (verbose) {
					System.out.println("ok");	
					//System.out.print("| Modification des flux ... ");
				}
				
				//s = Solution.modifyFlow(s, eT , degDivF);
				
				if (verbose) {
					//System.out.println("ok");
					System.out.print("| Initialisation ... ");
				}
				
				Solution.init(s, eT);
				Solution.reduce(s, eT);
				
				if (verbose) {
					System.out.println("ok");
					System.out.print("| Ecriture ... ");
				}
				
				oldScore = s.getValue();
				newScore = s.getValue();
				
				sm.addToExplored(s);
				ws = sm.revertToOriginalOrder(s);
				Solution.writeSolution(ws, eT, outFileRad+"_"+it+"d", "sol");
				
				if (verbose) {
					System.out.println("ok");
					System.out.println("| Score : "+newScore);
					System.out.println("[fait]\n");
				}
				
				cptI = lgExploPlateau;
				cptD--;
			}
				
			if (s.getValue() <= bs.getValue()) {
				bs = s;
			}				
		}

//******************************************************************//
// * Sauvegarde meilleure solution *                                //
//******************************************************************//
		
		System.out.println("Sauvegarde de la meilleure solution ...");
		System.out.println("| Score final : "+bs.getValue());
		System.out.print("| Ecriture ... ");
		ws = sm.revertToOriginalOrder(bs);
		Solution.writeSolution(ws, eT, outFileRad+"_BEST", "sol");
		System.out.println("ok");
		System.out.println("[fait]\n");
		
		System.out.println("[FIN : "+Duration.between(t0, LocalDateTime.now()).getSeconds()+"s]");
	}
}