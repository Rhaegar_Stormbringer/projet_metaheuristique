package graph;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Graph {
	//Attributs
	private int[][][] matrice;

	//Constructeur
	public Graph(String srcPath) {
		//Retrieve graph description
	}
	
	public Graph(List<String> graphDescription) {
		//Initialisation :
		Matcher match = null;
		Iterator<String> iter = graphDescription.iterator();
		String iterValue = null;

		//récupère le format
		match = patternMatch(iter.next(),"([0-9]+) ([0-9]+)");
		if (match.find()) {
			int size = Integer.parseInt(match.group(1));
			this.matrice = new int[size][size][3];
		} else {
			System.err.println("ERROR : Format mismatch!");
		}
		
		//Remplissage
		while (iter.hasNext()) {
			iterValue = iter.next();
			if (!iterValue.substring(0, 1).equals("c")) {
				match = patternMatch(iterValue, "([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+)\\.[0-9]* ([0-9]+)\\.[0-9]*");
				if (match.find()) {
					int nodeFrom = Integer.parseInt(match.group(1));
					int nodeTo   = Integer.parseInt(match.group(2));
					int date = 0;
					try {
						date     = Integer.parseInt(match.group(3));
					} catch(NumberFormatException e) {
						date = Integer.MAX_VALUE;
					}
					int lgth     = Integer.parseInt(match.group(4)) +1;
					int capa     = Integer.parseInt(match.group(5));
					
					this.matrice[nodeFrom][nodeTo][0] = date;
					this.matrice[nodeFrom][nodeTo][1] = lgth;
					this.matrice[nodeFrom][nodeTo][2] = capa;
					
					this.matrice[nodeTo][nodeFrom][0] = date;
					this.matrice[nodeTo][nodeFrom][1] = lgth;
					this.matrice[nodeTo][nodeFrom][2] = capa;					
				} else {
					System.err.println("ERROR : Format mismatch!");
				}
			}
		}
	}
	
	
	
	
	
	
	
	
	// Methodes
	
	/*
	 * Retourne le premier motif de source qui valide regex.
	 * Retourne null en cas d'échec.
	 */
	private Matcher patternMatch(String source, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(source);
		return matcher;
	}

	public int sizeOf() {
		return this.matrice.length;
	}
	
	public int[] findArc(int i, int o) {
		return this.matrice[i][o];
	} 
}
