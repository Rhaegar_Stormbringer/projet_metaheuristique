package graph;

public class Arc {
	private int sourceName;
	private int destinationName;
	private int dueDate;
	private int length;
	private int capacity;
	
	public Arc(int srcN, int dstN, int dD, int l, int c) {
		this.sourceName = srcN;
		this.destinationName = dstN;
		this.dueDate = dD;
		this.length = l;
		this.capacity = c;
	}
}
