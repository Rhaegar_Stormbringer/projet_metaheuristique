package graph;

import java.util.List;

public class Node {
	private int name;
	private List<Arc> arcs;

	public Node(int name) {
		this.name = name;
		this.arcs = null;
	}
	
	public void addArc(int destination, int dueDate, int length, int capacity) {
		this.arcs.add(new Arc(this.name, destination, dueDate, length, capacity));
	}
}
