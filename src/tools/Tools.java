package tools;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import verifier.EvacuationRoad;
import verifier.EvacuationTree;

public class Tools {
	
//=================================================================//
//  * Attributes *                                                 //
//=================================================================//
	
//=================================================================//
//  * Constructors *                                               //
//=================================================================//
	
//=================================================================//
//  * Statics *                                                    //
//=================================================================//	

static public int borneInf(EvacuationTree evacTree, String outputFileName, String outputExtensionName) {
	int criticalDuration = 0;
	for (EvacuationRoad eR : evacTree.getRoads()) {
		criticalDuration = Math.max(criticalDuration, eR.getDuration());
	}
	
	String fileName = "";
	if (outputFileName.equals("")) {
		fileName = "InstancesInt/"+evacTree.getGraph().getFileName().replaceAll(".full", "."+outputExtensionName);
	}
	else {
		fileName = "InstancesInt/"+outputFileName+"."+outputExtensionName;
	}
	try {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "utf-8"));
		writer.write(evacTree.getGraph().getFileName().replaceAll(".full", "")+"\n"+Integer.toString(evacTree.getRoads().size())+"\n");
		
		for (EvacuationRoad eR : evacTree.getRoads()) {
			writer.write(Integer.toString(eR.getFirst().getFrom())+" "+Integer.toString(eR.minFlow())+" 0\n");
		}
		
		writer.write("invalid\n"+criticalDuration+"\n-1\nTools.borneInf 06/06\nEvacuation from start; no constraint check");
		writer.close();
	}
	catch (IOException e) {
		e.printStackTrace();
	}
	
	//TBD Tristan
	/*
	 * format :
	 * EvacuationGraph.getFilename()+"_borneInf"
	 * count(EvacuationTree.getRoads())
	 * sommet_a_evacuer_id road.minFlow() 0
	 * ...
	 * "invalid"
	 * criticalDuration
	 * -1
	 * "Tools.borneInf 14/05"sol
	 * *Evacuation from start, no constraint check."
	 */
	
	return criticalDuration;
	
}

static public int borneSup(EvacuationTree evacTree, String outputFileName, String outputExtensionName) {
	int objectif = 0;
	String fileName = "";
	if (outputFileName.equals("")) {
		fileName = "InstancesInt/"+evacTree.getGraph().getFileName().replaceAll(".full", "."+outputExtensionName);
	}
	else {
		fileName = "InstancesInt/"+outputFileName+"."+outputExtensionName;
	}
	try {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "utf-8"));
		writer.write(evacTree.getGraph().getFileName().replaceAll(".full", "")+"\n"+Integer.toString(evacTree.getRoads().size())+"\n");
		
		for (EvacuationRoad eR : evacTree.getRoads()) {
			writer.write(Integer.toString(eR.getFirst().getFrom())+" "+Integer.toString(eR.minFlow())+" "+Integer.toString(objectif)+"\n");
			objectif += eR.getDuration();
		}
		
		writer.write("valid\n"+objectif+"\n-1\nTools.borneSup 14/05\nSequential evacuation; no constraint from dueDate");
		writer.close();
	}
	catch (IOException e) {
		e.printStackTrace();
	}
	//TBD Tristan
	/* format : 
	 * EvacuationGraph.getFilename()+"_borneSup"
	 * count(EvacuationTree.getRoads()) 
	 */
	
	/* "valid"
	 * objectif
	 * -1
	 * "Tools.borneSup 14/05"
	 * "Sequential evacuation; no constraint from dueDate."
	 */
	
	return objectif;
}

/*
 * Retourne le premier motif de source qui valide regex.
 * Retourne null en cas d'échec.
 */
	public static Matcher patternMatch(String source, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(source);
		return matcher;
	}

	public static final void swapInt(int[] a, int i, int j) {
		int t = a[i];
		a[i] = a[j];
		a[j] = t;	
	}

	
	public static final double log2(double a) {
		return Math.log(a)/Math.log(2);		
	}
}
