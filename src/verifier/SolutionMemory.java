package verifier;

import java.util.ArrayList;

import tools.Tools;

public class SolutionMemory {

//=================================================================//
//  * Attributes *                                                 //
//=================================================================//
	
	private int[]               initialOrder;
	private ArrayList<Solution> knownSolutions;
	private ArrayList<Solution> exploredSolutions;
	
//=================================================================//
//  * Constructors *                                               //
//=================================================================//
	
	public SolutionMemory(Solution s) {
		this.initialOrder = s.getNodeOrder().clone();
		this.knownSolutions = new ArrayList<Solution>();
		this.exploredSolutions = new ArrayList<Solution>();
	}
	
//=================================================================//
//  * Methods *                                                    //
//=================================================================//
	
	public boolean isKnown(Solution s) {
		for(Solution k : this.knownSolutions) {
			if ( (k.getNodeOrder().equals(s.getNodeOrder())) && (k.getNodeFlow().equals(s.getNodeFlow())) ){
				return true;
			}
		}
		return false;
	}
		
	public boolean isExplored(Solution s) {
		for(Solution k : this.exploredSolutions) {
			if ( (k.getNodeOrder().equals(s.getNodeOrder())) && (k.getNodeFlow().equals(s.getNodeFlow())) ){
				return true;
			}
		}
		return false;
	}
	
//=================================================================//

	public Solution revertToOriginalOrder(Solution s) {
		Solution out = new Solution(s);
		int j = 0;
		for (int i = 0 ; i < this.initialOrder.length ; i++) {
			j = s.getIndex(this.initialOrder[i]);
			if (j != -1) {
				out.getNodeOrder()[i] = s.getNodeOrder()[j];
				out.getNodeFlow()[i]  = s.getNodeFlow()[j];
				out.getNodeStart()[i] = s.getNodeStart()[j];
			} else {
				System.err.println("Cannot revert to original order : list content doesn't match.");
				System.exit(-1);
			}			
		}
		return out;
	}
	
//=================================================================//

	public void addToKnown(Solution s) {
		if (!this.knownSolutions.contains(s)) {
			this.knownSolutions.add(s);
		}
	}
	
	public void addToKnown(ArrayList<Solution> aS) {
		for ( Solution s : aS) {
			this.addToKnown(s);
		}
	}
	
	public void addToExplored(Solution s) {
		if (!this.exploredSolutions.contains(s)) {
			this.exploredSolutions.add(s);
			this.addToKnown(s);
		}
	}
	
	public void addToExplored(ArrayList<Solution> aS) {
		for ( Solution s : aS) {
			this.addToExplored(s);
		}
	}
	
//=================================================================//	
	
	public ArrayList<Solution> removeMemorizedSolutionsFrom(ArrayList<Solution> aS) {
		ArrayList<Solution> out = new ArrayList<Solution>();
		boolean add = true;
		for (Solution s : aS) {
			add = true;
			for (Solution t : this.exploredSolutions) {
				if (( s.equals(t) )) {
					add = false;
				}
			}
			
			if (add) {
				out.add(s);
			}
		}

		return out;
	}
	
//=================================================================//
	
	public int sizeKnown() {
		return this.knownSolutions.size();
	}
	
	public int sizeExplored() {
		return this.exploredSolutions.size();
	}
	
}
