package verifier;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EvacuationGraph {
	
//=================================================================//
//  * Attributes *                                                 //
//=================================================================//
	private Segment tab[][];
	private String fileName;	
	
//=================================================================//
//  * Constructors *                                               //
//=================================================================//
	
	public EvacuationGraph(String fileName) {
		this(EvacuationGraph.getFileContentThenLeave(fileName));
		this.fileName = fileName;
	}
	
	public EvacuationGraph(List<String> graphDescription) {
		/*
		 * Reconstruction de la List<String> en List<List<<String>> a l'aide de la methode split :
		 * exemple 1er element : 549 930 devient [549,930] 
		 */
		List<List<String>> stuff = new ArrayList<List<String>>();
		for (int i=0; i<graphDescription.size(); i++) {
			stuff.add(new ArrayList<String>(Arrays.asList(graphDescription.get(i).split(" "))));
		}
		
		/*
		 * Construction du graphe
		 */
		this.tab = new Segment[stoi(stuff.get(0).get(0))][stoi(stuff.get(0).get(1))];
		int nodeFrom, nodeTo, date, lgth, capa;
		for (int i=1; i<stuff.size(); i++) {
			nodeFrom = stoi(stuff.get(i).get(0));
			nodeTo   = stoi(stuff.get(i).get(1));
			date = 0;
			try {
				date     = stoi(stuff.get(i).get(2));
			} catch(NumberFormatException e) {
				date = Integer.MAX_VALUE;
			}
			lgth = stoi(stuff.get(i).get(3));
			capa = stoi(stuff.get(i).get(4));
			this.tab[nodeFrom][nodeTo] = new Segment(nodeFrom, nodeTo, date, capa, lgth);
			this.tab[nodeTo][nodeFrom] = new Segment(nodeTo, nodeFrom, date, capa, lgth);
		}
	}
	
	
//=================================================================//
//  * Methods *                                                    //
//=================================================================//
	public static List<String> getFileContentThenLeave(String fileName) {
		/*
		 * Recuperation du contenu du fichier
		 */
		StringBuilder contentBuilder = new StringBuilder();
	    try {
	    	BufferedReader br = new BufferedReader(new FileReader("InstancesInt/"+fileName));
	        String line;
	        while ((line = br.readLine()) != null)
	        {
	            contentBuilder.append(line).append("\n");
	        }
	        br.close();
	    }
	    catch (IOException e) {
	        e.printStackTrace();
	    }
	    List<String> content = new ArrayList<String>(Arrays.asList(contentBuilder.toString().split("\n")));
	    

		/*
		 * Suppression de tout le contenu pre-graphe (evacuation info)
		 */
	    content.remove(0);
	    while (!content.get(0).contains("c")) {
	    	content.remove(0);
	    }
	    content.remove(0);
	    return content;
	}
	
	public String getFileName() {
		return this.fileName;
	}
	
	public Segment getSegment(int from, int to) {
		return this.tab[from][to];
	}
	
	private int stoi(String s) {
		return Integer.parseInt(s);
	}
}
