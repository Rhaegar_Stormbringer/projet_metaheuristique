package verifier;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sun.corba.se.impl.orbutil.graph.Graph;

import tools.Tools;

public class EvacuationTree {
	
//=================================================================//
//  * Attributes *                                                 //
//=================================================================//
	private ArrayList<SourceSegment> sources;
	private ArrayList<Segment>       safety;
	
	private EvacuationGraph graph;
	private ArrayList<EvacuationRoad> roads;
	
	private boolean valid;
	
//=================================================================//
//  * Constructors *                                               //
//=================================================================//
	
	public EvacuationTree(String fileName) {
		//Initialization
		Segment s = null;
		SourceSegment ss = null;
		roads = new ArrayList<EvacuationRoad>();
		sources = new ArrayList<SourceSegment>();
		safety = new ArrayList<Segment>();
		
		//Create the graph (use constructor)
		this.graph = new EvacuationGraph(fileName);
		
		//Use the file to create all the roads :
		List<String> evacInfo = getEvacuationInfoFromFile(fileName);
		for (int i=0; i < evacInfo.size(); i++) {
			this.roads.add(new EvacuationRoad(this.graph, evacInfo.get(i)));
		}
		
		
		//Select the SourceSegment and safety Segment
		for (EvacuationRoad r : roads) {
			//SourceSegment
			ss = r.getFirst();
			r.getRoad().remove(0);
			r.getRoad().add(0, ss);
			
			if (!sources.contains(ss)) {
				sources.add(ss);
			}
			
			//Safety Segment
			s = r.getLast();
			if (!safety.contains(s)) {
				safety.add(s);
			}			
		} 
		
		
	}
	
//=================================================================//
//  * Methods *                                                    //
//=================================================================//	
	
	/* tik()
	 * Drive the system 1 tik forward.
	 * Return whether the system is still valid or not.
	 */
	public boolean tik() {
		boolean out = true;
		for (Segment s : safety) {
			out &= s.tik();
		}
		return out;
	}
	
	/* tok()
	 * Determine if the system is valid at an instant t.
	 */
	public boolean tok(int t) {
		boolean out = true;
		for (Segment s : safety) {
			out &= s.tok(t);
		}
		return out;
	}	
	
	/* apply()
	 * Apply a solution file to the system
	 */
	public void apply(String fileName) {
		//Retrieve the line of the solution file given by filename
		List<String> sol = getSolutionFromFile(fileName);
		
		//Add the flow and evacuationDate to each SourceSegment
		for (String s : sol) {
			String[] stuff = s.split(" ");
			for (int i=0; i<sources.size(); i++) {
				if (stuff[0].equals(itos(sources.get(i).getFrom())))  {
					sources.set(i, new SourceSegment(sources.get(i), sources.get(i).getNumberOfCars(), stoi(stuff[1]), stoi(stuff[2])));
					break;
				}
			}
		}
		/*
		for (SourceSegment ss : sources) {
			System.out.println(ss.getFrom()+" | "+ss.getTo()+" | "+ss.getNumberOfCars()+" | "+ss.getFlow()+" | "+ss.getEvacuationDate());
		}
		*/
		
		//Clear the tasks
		Task.clearTasks(this);
		
		//Recreate the tasks
		Task.generateTasks(this);
	}
	
	/* apply()
	 * Apply a solution Solution
	 */
	public void apply(Solution s) {
		//Add the flow and evacuationDate to each SourceSegment
		for (SourceSegment ss : this.sources) {
			int index = s.getIndex(ss.getFrom());
			ss.setFlow(s.getNodeFlow()[index]);
			ss.setEvacuationDate(s.getNodeStart()[index]);
		}			
		
		//Clear the tasks
		Task.clearTasks(this);
		
		//Recreate the tasks
		Task.generateTasks(this);
	}
	
	
	
	
	
	
	/* verifyCalc()
	 * verify the solution through the tok() method.
	 */
	public int verifyCalc() {
		//Initialization
		int done = 0;
		int instant = 0;
		boolean globalValidity = true;
		
		//Retrieve last instant
		for(Segment s : safety) {
			for(Task t : s.getTaskList()) {
				if (done < t.getOffset()+ t.getDuration() + s.getLength()) {
					done = t.getOffset()+ t.getDuration() + s.getLength();
				}
			}			
		}
		
		//Verify validity for each instant i :
		while ((instant <= done)&&(globalValidity)) {
			globalValidity &= this.tok(instant);
			instant ++;
		}
		
		if (globalValidity) {
			this.valid = true;
		} else {
			this.valid = false;
		}
		
		//Output
		return done;
	}

//=================================================================//
//  * Getters *                                                    //
//=================================================================//	
	
	public ArrayList<SourceSegment> getSources() {
		return sources;
	}

	public ArrayList<Segment> getSafety() {
		return safety;
	}

	public EvacuationGraph getGraph() {
		return graph;
	}

	public ArrayList<EvacuationRoad> getRoads() {
		return roads;
	}

	public boolean isValid() {
		return valid;
	}
	
	public EvacuationRoad findRoad(int s) {
		for (EvacuationRoad er : this.roads) {
			if (er.getFirst().getFrom() == s) {
				return er;
			}
		}
		return null;
	}

//=================================================================//
//  * Tools *                                                      //
//=================================================================//
	
	private int stoi(String s) {
		return Integer.parseInt(s);
	}
	
	private String itos(int i) {
		return Integer.toString(i);
	}
	
	/* getEvacuationInfoFromFile()
	 * Return the c [evacuation info] of a .full file in the form of a List<String>
	 * One line of the file -> one String
	 */
	private static List<String> getEvacuationInfoFromFile(String fileName) {
		/*
		 * Recuperation du contenu du fichier
		 */
		StringBuilder contentBuilder = new StringBuilder();
	    try {
	    	BufferedReader br = new BufferedReader(new FileReader("InstancesInt/"+fileName));
	        String line;
	        while ((line = br.readLine()) != null)
	        {
	            contentBuilder.append(line).append("\n");
	        }
	        br.close();
	    }
	    catch (IOException e) {
	        e.printStackTrace();
	    }
	    List<String> tmp = new ArrayList<String>(Arrays.asList(contentBuilder.toString().split("\n")));
	    List<String> content = new ArrayList<String>();

		/*
		 * Suppression de tout le contenu post-evacuation info (graphe)
		 */
	    tmp.remove(0);	// suppr ligne commentaire
	    tmp.remove(0);	// suppr header
	    while (!tmp.get(0).contains("c")) {
	    	content.add(tmp.remove(0));
	    }
	    return content;
	}

	private static List<String> getSolutionFromFile(String fileName) {
		StringBuilder contentBuilder = new StringBuilder();
	    try {
	    	BufferedReader br = new BufferedReader(new FileReader("InstancesInt/"+fileName));
	        String line;
	        while ((line = br.readLine()) != null)
	        {
	            contentBuilder.append(line).append("\n");
	        }
	        br.close();
	    }
	    catch (IOException e) {
	        e.printStackTrace();
	    }
	    List<String> tmp = new ArrayList<String>(Arrays.asList(contentBuilder.toString().split("\n")));
	    List<String> content = new ArrayList<String>();
		
	    tmp.remove(0);	// suprr ligne info
	    tmp.remove(0);	// suppr nom du fichier associe
	    tmp.remove(0);	// suppr header
	    while (!Tools.patternMatch(tmp.get(0), "[a-zA-Z]+").find()) {
	    	content.add(tmp.remove(0));
	    }
		return content;
	}
}
