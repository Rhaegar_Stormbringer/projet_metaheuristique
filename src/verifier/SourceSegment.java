package verifier;

public class SourceSegment extends Segment {

//=================================================================//
//  * New Attributes *                                             //
//=================================================================//
	
	private int numberOfCars;
	private int flow;
	
	private int evacuationDate;

//=================================================================//
//  * Constructors *                                               //
//=================================================================//
	
	//Straight forward
	public SourceSegment(int from, int to, int dueDate, int capacity, int length, int numberOfCars, int flow, int evacuationDate) {
		super(from, to, dueDate, capacity, length);
		this.numberOfCars = numberOfCars;
		this.flow = flow;
		this.evacuationDate = evacuationDate;
	}
	
	//From existing Segment
	public SourceSegment(Segment s, int numberOfCars, int flow, int evacuationDate) {
		super(s.getFrom(), s.getTo(), s.getPreviousSegments(), s.getNextSegments(), s.getDueDate(), s.getCapacity(), s.getLength(), 
				s.getState(), s.getCurrentDate(), s.isValidSimu() );
		this.numberOfCars = numberOfCars;
		this.flow = flow;		
		this.evacuationDate = evacuationDate;
	}

//=================================================================//
//  * Methods *                                                    //
//=================================================================//
	

	/* getNewCars
	 * Override the normal value with the flow value.
	 */
	@Override
	public int getNewCars() {
		
		int mem = Math.max(this.numberOfCars - this.flow, 0);
		int out = 0;
		
		if (this.currentDate >= this.evacuationDate) {
			out = this.numberOfCars - mem;
			this.numberOfCars = mem;
		} 
		
		return out;
	}
	
//=================================================================//
//  * Getters *                                                    //
//=================================================================//	
		
	public int getNumberOfCars() {
		return numberOfCars;
	}
	
	public int getFlow() {
		return flow;
	}

	public int getEvacuationDate() {
		return evacuationDate;
	}
	
	public void setFlow(int flow) {
		this.flow = flow;
	}

	public void setEvacuationDate(int evacuationDate) {
		this.evacuationDate = evacuationDate;
	}

}
