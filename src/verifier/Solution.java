package verifier;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import tools.Tools;
import tools.neighborsEnum;

public class Solution implements Comparable<Solution>{
	
//=================================================================//
//  * Attributes *                                                 //
//=================================================================//
	
	private int[] nodeOrder;
	private int[] nodeInitialFlow;
	private int[] nodeFlow;
	private int[] nodeStart;
	private int   value;
	private String methodName = "no method";
	
//=================================================================//
//  * Constructors *                                               //
//=================================================================//
	
	public Solution(Solution s) {
		this(s.getNodeOrder(), s.getNodeFlow(), s.getNodeStart());
	}

	public Solution(int[] nO, int[] nF, int[] nS) {
		this.nodeOrder       = nO.clone();
		this.nodeInitialFlow = nF.clone();
		this.nodeFlow        = nF.clone();
		this.nodeStart       = nS.clone();		
	}

	public Solution(int[] nO, int[] nF) {
		this.nodeOrder       = nO.clone();
		this.nodeInitialFlow = nF.clone();
		this.nodeFlow        = nF.clone();		
		this.nodeStart       = new int[nO.length];
	}
	
	public Solution(int[] nO) {
		this.nodeOrder = nO.clone();
		this.nodeInitialFlow = new int[nO.length];
		this.nodeFlow        = new int[nO.length];
		this.nodeStart       = new int[nO.length];
	}
	
//=================================================================//
//  * Methods *                                                    //
//=================================================================//
	
	public ArrayList<Solution> neighbors(neighborsEnum e) {
		if (e == neighborsEnum.ORDER) {
			this.methodName = "Order (basic)";
			return this.nOrder();
		} else if (e == neighborsEnum.FLOW) {
			this.methodName = "Order (flow)";
			return this.nFlow();
		} else if ( e == neighborsEnum.ALL ) {
			this.methodName = "Order (all)";
			return this.nAll();
		} else {
			return null;
		}
	}
	
	private ArrayList<Solution> nOrder() {
		ArrayList<Solution> neighborhood = new ArrayList<Solution>();
		
		//Generate the swaps :
		for (int i = 0 ; i < this.nodeOrder.length ; i++) {
			for (int j = i+1 ; j < this.nodeOrder.length ; j++) {
				int[] nO = this.nodeOrder.clone();
				int[] nF = this.nodeInitialFlow.clone();
				Tools.swapInt(nO, i, j);
				Tools.swapInt(nF, i, j);
				Solution s = new Solution(nO, nF);
				neighborhood.add(s);
				//System.out.println(s);
			}
		}
		return neighborhood;
	}
	
	private ArrayList<Solution> nFlow() {
		ArrayList<Solution> neighborhood = new ArrayList<Solution>();
		
		//Find min flow :
		int min = this.nodeFlow[0];
		for (int a : this.nodeFlow) {
			if (a <= min) {
				min = a;
			}
		}
		min = min -1;
		
		//Generate the decreased
		for (int i = 0 ; i < this.nodeOrder.length ; i++) {
			int[] nO = this.nodeOrder.clone();
			int[] nF = this.nodeFlow.clone();
			nF[i] = nF[i] - min;
			Solution s = new Solution(nO, nF);
			neighborhood.add(s);
		}
		
		return neighborhood;
	}
	
	private ArrayList<Solution> nAll() {
		ArrayList<Solution> neighborhood = new ArrayList<Solution>();
		ArrayList<Solution> n0 = this.nOrder();
		for (Solution s : n0) {
			neighborhood.add(s);
			neighborhood.addAll(s.nFlow());			
		}
		neighborhood.addAll(this.nFlow());
		return neighborhood;
	}

//=================================================================//
	
	public static void reduce(Solution s, EvacuationTree eT) {
		s.methodName = "reduce";
		int reducer = (int)(s.getValue()/2);
		//We reduce the date in priority order given by nodeOrder
		for (
			int i = 0 ; i < s.getNodeOrder().length ; i++) {
			//Reinitialize the reducer
			reducer = (int)(s.getValue()/2);
			
			//we try to reduce to the maximum
			while (reducer > 0) {
				//If we can reduce with this value
				if (s.getNodeStart()[i] - reducer >= 0 ) {
					//Verify if still valid
					s.getNodeStart()[i] = s.getNodeStart()[i] - reducer;
					eT.apply(s);
					eT.verifyCalc();
					if (!eT.isValid()) {
						s.getNodeStart()[i] = s.getNodeStart()[i] + reducer;
						//Decrease the reducer
						reducer = (int)(reducer/2);
					}				
				} else {
					//Decrease the reducer
					reducer = (int)(reducer/2);
				}
			}
		}
		
		eT.apply(s);
		s.setValue(eT.verifyCalc());
	}
	
	public static void reduce(ArrayList<Solution> aS, EvacuationTree eT) {
		for(Solution s : aS) {
			Solution.reduce(s, eT);
		}
	}
	
//=================================================================//
	
	public static Solution shuffle(Solution s, int deg) {
		
		if (deg <= 0) {
			deg = 5 * s.getNodeOrder().length;
		}
		
		Random r = new Random();
		int l[] = s.getNodeOrder().clone();
		int m[] = s.getNodeFlow().clone();
		int o[] = s.getNodeStart().clone();
		int i,j;
		
		while (deg > 0) {
			i = r.nextInt(s.getNodeOrder().length);
			j = r.nextInt(s.getNodeOrder().length);
			Tools.swapInt(l, i, j);		
			Tools.swapInt(m, i, j);
			Tools.swapInt(o, i, j);
			deg--;
		}
		
		return new Solution(l, m, o);
		
		
	}
	
	public static Solution modifyFlow(Solution s, EvacuationTree eT, int deg) {
		int nO[] = s.getNodeOrder().clone();
		int nF[] = s.getNodeInitialFlow().clone();
		int nS[] = s.getNodeStart().clone();
		Random r = new Random();
		
		if (deg <= 0) {
			for (int i = 0 ; i < s.getNodeFlow().length ; i++ ) {
				nF[i] = 1 + r.nextInt(eT.findRoad(nO[i]).minFlow()-1);
			}
		} else {
			for (int i = 0 ; i < s.getNodeFlow().length ; i++ ) {
				nF[i] = Math.max(1, nF[i]-deg);
			}
		}
		
		
		return new Solution(nO, nF, nS);
		
		
	}
	
	
//=================================================================//
	
	public static void place(Solution s, EvacuationTree eT) {
		s.methodName = "place";
		//Initialisation :
		boolean reduce = true;
		
		//Placement des  nodes :
		for (int i = 0 ; i < s.getNodeOrder().length ; i++) {
			
			//Réinitialisation :
			reduce = true;
			s.getNodeStart()[i] = 0;
			
			//Placement :
			while (reduce) {
				eT.apply(s);
				eT.verifyCalc();
				if (eT.isValid()) {
					reduce = false;
				} else {
					s.getNodeStart()[i]++;
				}
			}
		}
		
		eT.apply(s);
		s.setValue(eT.verifyCalc());
	}

	public static void place(ArrayList<Solution> aS, EvacuationTree eT) {
		for(Solution s : aS) {
			Solution.place(s, eT);
		}
	}

//=================================================================//

	public static void adjust(Solution s, EvacuationTree eT) {
		//Initialisation :
		boolean maximize = true;
		
		//Maximisation des flux
		for (int i = s.getNodeOrder().length-1 ; i >= 0 ; i--) {
			
			//Réinitialisation :
			maximize = true;
			
			while (maximize) {
				s.getNodeFlow()[i]++;
				eT.apply(s);
				eT.verifyCalc();
				if (!eT.isValid()) {
					s.getNodeFlow()[i]--;
					maximize = false;
				}
			}
		}
		
		eT.apply(s);
		s.setValue(eT.verifyCalc());
	}
	
	public static void adjust(ArrayList<Solution> aS, EvacuationTree eT) {
		for(Solution s : aS) {
			Solution.adjust(s, eT);
		}
	}
	
//=================================================================//
	
	public static void init(Solution s, EvacuationTree eT) {
		int date = 0;
		for (int i = 0 ; i < s.getNodeOrder().length ; i++) {
			s.getNodeFlow()[i] = eT.findRoad(s.getNodeOrder()[i]).minFlow();
			s.getNodeStart()[i] = date;
			date += eT.findRoad(s.getNodeOrder()[i]).getDuration();
		}
		s.setValue(date+1);
	}
	
	public static void init(ArrayList<Solution> aS, EvacuationTree eT) {
		for (Solution s : aS) {
			Solution.init(s, eT);
		}
	}
			
 //=================================================================//
	
	public static void writeSolution(Solution s, EvacuationTree eT, String outputFileName, String outputExtensionName) {
		String fileName = "";
		String validity = "";
		if (outputFileName.equals("")) {
			fileName = "InstancesInt/"+eT.getGraph().getFileName().replaceAll(".full", "."+outputExtensionName);
		}
		else {
			fileName = "InstancesInt/"+outputFileName+"."+outputExtensionName;
		}
		
		if (eT.isValid()) {
			validity = "valid";
		}
		else {
			validity = "invalid";
		}
		
		try {
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "utf-8"));
			writer.write(eT.getGraph().getFileName().replaceAll(".full", "")+"\n"+Integer.toString(eT.getRoads().size())+"\n");
			
			for (int i=0; i < s.nodeOrder.length; i++) {
				writer.write(s.nodeOrder[i]+" "+s.nodeFlow[i]+" "+s.nodeStart[i]+"\n");
			}
			writer.write(validity+"\n"+"-1\n"+s.methodName+"\ncomment on the method used\n");
			writer.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

//=================================================================//
//  * Get/Set *                                                    //
//=================================================================//
	public int[] getNodeOrder() {
		return nodeOrder;
	}

	public void setNodeOrder(int[] nodeOrder) {
		this.nodeOrder = nodeOrder;
	}

	public int[] getNodeFlow() {
		return nodeFlow;
	}

	public void setNodeFlow(int[] nodeFlow) {
		this.nodeFlow = nodeFlow;
	}

	public int[] getNodeStart() {
		return nodeStart;
	}

	public void setNodeStart(int[] nodeStart) {
		this.nodeStart = nodeStart;
	}
	
	public int getIndex(int node) {
		for (int i = 0 ; i < this.nodeOrder.length ; i++) {
			if (this.nodeOrder[i] == node) {
				return i;
			}
		}
		return -1;
	}
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}


//=================================================================//
//* Overwrite *                                                    //
//=================================================================//

	public int[] getNodeInitialFlow() {
		return nodeInitialFlow;
	}

	public void setNodeInitialFlow(int[] nodeInitialFlow) {
		this.nodeInitialFlow = nodeInitialFlow;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	@Override
	public int compareTo(Solution s) {
		return Integer.compare(this.value, s.getValue());
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (!(o instanceof Solution)) {
			return false;
		}
		
		
		Solution s = (Solution) o;
		return (   (Arrays.equals(this.nodeOrder, s.getNodeOrder()))
			    && (Arrays.equals(this.nodeFlow,  s.getNodeFlow() ))  );		
	}
	
	
	@Override
	public String toString() {
		String s = "\n[";
		for (int i : this.nodeOrder) {
			s += Integer.toString(i)+" ";
		}
		s += "] ("+Integer.toString(this.nodeOrder.length)+")\n[";
		for (int j : this.nodeFlow) {

			s += Integer.toString(j)+" ";
		}
		s += "] ("+Integer.toString(this.nodeFlow.length)+")\n[";
		for (int k : this.nodeStart) {

			s += Integer.toString(k)+" ";
		}
		s += "] ("+Integer.toString(this.nodeStart.length)+")\n";
		return s;
	}
}
