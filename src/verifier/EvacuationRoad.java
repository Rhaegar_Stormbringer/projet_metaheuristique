package verifier;

import java.util.ArrayList;
import java.util.List;

import verifier.EvacuationGraph;

// 227 2483 71 19 237 244 57 529 548 70 87 111 503 382 531 225 481 489 493 437 385 346 373

public class EvacuationRoad {
//=================================================================//
//  * Attributes *                                                 //
//=================================================================//
	
	private List<Segment> road;
	
//=================================================================//
//  * Constructors *                                               //
//=================================================================//
	
	public EvacuationRoad(EvacuationGraph graph, String instance) {
		this.road = new ArrayList<Segment>();
		
		/*
		 *  separation du string initial, on a alors :
		 *  stuff[0] : id du noeud a evacuer
		 *  stuff[1] : nombre de voitures presentent sur le noeud initial
		 *  stuff[2] : max rate d'evacuation
		 *  stuff[3] : nombre de noeuds dans le chemin d'evacutation
		 *  stuff[4...n] : id d'un noeud
		 */
		String[] stuff = instance.split(" ");
		
		// "creation" (modification et recuperation) du SourceSegment ...
		this.road.add(new SourceSegment(graph.getSegment(stoi(stuff[0]), stoi(stuff[4])), stoi(stuff[1]), 0, 0));
		
		// ... puis recuperation des Segments suivants
		for (int i=4; i < (stuff.length-1); i++)
		{
			this.road.add(graph.getSegment(stoi(stuff[i]), stoi(stuff[i+1])));
			//System.out.println("["+i+"] --> "+stuff[i]+" | "+stuff[i+1]);
		}
		
		/*
		 * Pour chaque segment de la route, remplissage de ses listes previousSegments
		 * et nextSegments en repassant sur la route nouvellement construite 
		 */
		for (int i=0; i < (this.road.size()); i++) {
			Segment s = this.road.get(i);
			if (i == 0) {
				s.addNext(this.road.get(i+1));
			}
			else if (i == this.road.size()-1) {
				s.addPrevious(this.road.get(this.road.size()-2));
			}
			else {
				s.addPrevious(this.road.get(i-1));
				s.addNext(this.road.get(i+1));
			}
			//System.out.println(s.from+" | "+s.to+" | "+s.getPreviousSegments().size()+" | "+s.getNextSegments().size());
		}
	}
	
//=================================================================//
//  * Methods *                                                    //
//=================================================================//
	
	public SourceSegment getFirst() {
		return (SourceSegment) this.road.get(0);
	} 
	
	public Segment getLast() {
		return this.road.get(this.road.size()-1);
	}
	
	public int getDuration() {
		int duration = 0;
		int minFlow = this.minFlow();	
		
		for (Segment s : this.road) {
			duration += s.getLength();
		}
		
		duration += this.getFirst().getNumberOfCars()/minFlow;
		
		return duration;
	}
	
	public int minFlow() {
		int minFlow  = this.getFirst().getCapacity();
		for (Segment s : this.road) {
			minFlow = Math.min(minFlow, s.getCapacity());
		}		
		return minFlow;
	}
//=================================================================//
//  * Getters *                                                    //
//=================================================================//	

	public List<Segment> getRoad() {
		return this.road;
	}

//=================================================================//
//  * Tools *                                                      //
//=================================================================//
	
	private int stoi(String s) {
		return Integer.parseInt(s);
	}
}
