package verifier;

import java.util.ArrayList;

public class Segment {
	
//=================================================================//
//  * Attributes *                                                 //
//=================================================================//
	
	protected int from;
	protected int to;
	
	protected ArrayList<Segment> previousSegments = null; //Used segment, not all
	protected ArrayList<Segment> nextSegments = null; //Used segment, not all
	
	protected int dueDate;
	protected int capacity;
	protected int length;
	
	protected int state[];
	protected int currentDate = 0;
	
	protected boolean validSimu = true;
	protected boolean validCalc = true;
	
	protected ArrayList<Task> taskList = null;
	
//=================================================================//
//  * Constructors *                                               //
//=================================================================//
	
	//Minimal
	public Segment(int from, int to, int dueDate, int capacity, int length) {
		this.from = from;
		this.to = to;
		this.previousSegments = new ArrayList<Segment>();
		this.nextSegments = new ArrayList<Segment>();
		this.dueDate  = dueDate;
		this.capacity = capacity;
		this.length   = length;
		
		this.state = new int[length];
		for(int i = 0 ; i < length ; i++) {
			this.state[i] = 0;
		}
		
		this.taskList = new ArrayList<Task>();
	} 	
	
	//Omniscient
	public Segment(int from, int to,
			ArrayList<Segment> previousSegments, ArrayList<Segment> nextSegments, 
			int dueDate, int capacity,    int length,
			int[] state, int currentDate, boolean validSimu) {
		
		this.from = from;
		this.to = to;
		this.previousSegments = previousSegments;
		this.nextSegments = nextSegments;
		this.dueDate = dueDate;
		this.capacity = capacity;
		this.length = length;
		this.state = state;
		this.currentDate = currentDate;
		this.validSimu = validSimu;
		
		this.taskList = new ArrayList<Task>();
	}
	
//=================================================================//
//   * Methods *                                                   //
//=================================================================//
	
	/* tik() [rec]
	 * Drive the system 1 tik forward.
	 * Return whether the system is still valid or not.
	 */
	public boolean tik() {
		
		//Increase the current date
		this.currentDate++;
				
		//Drag the cars along the road
		for(int i = length ; i > 0 ; i--) {
			this.state[i] = this.state[i-1];
		}
		
		//New cars enter
		this.state[0] = this.getNewCars();
		
		//Get the new validity
		this.validSimu &= (this.state[0] <= this.capacity);
	
		//Update previous segments :
		for(Segment s : this.previousSegments) {
			if (this.validSimu) {
				this.validSimu &= s.tik();
			} 
		}

		//Output
		return this.validSimu;
	}

	/* tok() [rec]
	 * Determine if the segment is valid at an instant t;
	 * A segment is valid if all is previous segments are valid
	 * and the occupation does not exceed the capacity
	 */
	public boolean tok(int t) {
		//Initialisation
		int occupation = 0;
		this.validCalc = true;
		
		//Calculate the occupation a time t
		for(Task tsk : this.taskList) {
			occupation += tsk.getFlow(t);
		}
		
		//Modify the validity knowing the occupation
		this.validCalc &= (occupation <= capacity);
		
		//Modify the validity regarding the previous segments :
		for (Segment s : this.previousSegments) {
			if(this.validCalc) {
				this.validCalc &= s.tok(t);
				//System.out.println("validCalc ? "+this.from+" | "+this.to+" | "+this.validCalc);
			}
		}
		
		//Return
		return this.validCalc;
		
	}
	
	/* getState()
	 * get the value of the current 
	 * occupation of the segment at 
	 * the requested index.
	 * -1 get the last.
	 */
	public int getState(int index) {
		int out = -1;
		if (index == -1) {
			out = this.state[length-1];
		} else {
			out = this.state[index];
		}
		return out;
	}
	
	/* getNewCars()
	 * get the amount of car that comes
	 * from previous segments.
	 */
	public int getNewCars() {
		int out = 0;
		for(Segment s : this.previousSegments) {
			out += s.getState(-1);
		}
		return out;
	}
	
	/* clearTasks()
	 * Remove all the tasks
	 */
	public void clearTasks() {
		this.taskList.clear();
	}

//=================================================================//
//  * Getters *                                                    //
//=================================================================//
	
	public int getFrom() {
		return from;
	}

	public int getTo() {
		return to;
	}

	public ArrayList<Segment> getPreviousSegments() {
		return previousSegments;
	}

	public ArrayList<Segment> getNextSegments() {
		return nextSegments;
	}
	
	public Segment getNextSegment() {
		return nextSegments.get(0);
	}

	public int getDueDate() {
		return dueDate;
	}

	public int getCapacity() {
		return capacity;
	}

	public int getLength() {
		return length;
	}

	public int[] getState() {
		return state;
	}

	public int getCurrentDate() {
		return currentDate;
	}
	
	public ArrayList<Task> getTaskList() {
		return taskList;
	}
	
	public boolean isValidSimu() {
		return validSimu;
	}
	
	public boolean isValidCalc() {
		return validCalc;
	}

	
//=================================================================//
//  * Override *                                                   //
//=================================================================//
	
	@Override
	public boolean equals(Object o) {
		//Trivial case
		if (o == this) {
			return true;
		}
		
		//Not a Segment
		if (!(o instanceof Segment)) {
			return false;
		}
		
		Segment s = (Segment) o;
		return ((this.getTo() == s.getTo())&&(this.getFrom() == s.getFrom()));
		
	}
	
//=================================================================//
//  * Adders *                                                     //
//=================================================================//	
	
	public void addPrevious(Segment s) {
		if (!this.previousSegments.contains(s)) {
			this.previousSegments.add(s);
		}
	}
	
	public void addNext(Segment s) {
		this.nextSegments.add(s);
	}
	
	public void addTask(Task t) {
		this.taskList.add(t);
	}

	
}
