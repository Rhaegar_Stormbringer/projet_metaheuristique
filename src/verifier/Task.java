package verifier;

public class Task {

//=================================================================//
//  * Attributes *                                                 //
//=================================================================//

	private int sourceNode;
	
	private int flow;
	private int residualFlow;
	
	private int duration;
	private int offset;
	
//=================================================================//
//  * Constructors *                                               //
//=================================================================//

	public Task(SourceSegment s) {
		this.sourceNode   = s.getFrom();
		this.flow         = s.getFlow();
		this.residualFlow = s.getFlow();
		this.duration     = s.getNumberOfCars()/s.getFlow();
		this.offset       = s.getEvacuationDate();
		
		if (s.getNumberOfCars()%s.getFlow() != 0) {
			this.duration = (int)Math.floor(this.duration) +1;
			this.residualFlow = s.getNumberOfCars()%s.getFlow();
		}
	}

	public Task(int sourceNode, int flow, int residualFlow, int duration, int offset) {
		this.sourceNode = sourceNode;
		this.flow = flow;
		this.residualFlow = residualFlow;
		this.duration = duration;
		this.offset = offset;
	}

//=================================================================//
//* Methods *                                                      //
//=================================================================//

	/* generateTasks() [static]
	 * generate all the task of all the arc of the EvacuationTree
	 */
	public static void generateTasks(EvacuationTree tree) {
		//Initialisation
		Segment currentS, nextS;
		Task    currentT, nextT;
		
		//Add the tasks of SourceNode
		for(SourceSegment ss : tree.getSources()) {
			//Init
			currentT = new Task(ss);
			currentS = ss;
			ss.addTask(currentT);
			
			//Iter on the next (pass the task to the next steps)
			while (!currentS.getNextSegments().isEmpty()) {
				nextS = currentS.getNextSegment();
				nextT = Task.delayedTask(currentT, currentS);
				nextS.addTask(nextT);
				
				currentT = nextT;
				currentS = nextS;
			}
		}
		
	}

	/* DelayedTask() [static] 
	 * Create a new Task from an existing one
	 * while adding the delay due to the length of
	 * the previous segment. 
	 */
	private static Task delayedTask(Task previousTask, Segment previousSegment) {
		return new Task(previousTask.getSourceNode(), 
				        previousTask.getFlow(), 
				        previousTask.getResidualFlow(), 
				        previousTask.getDuration(), 
				        previousTask.getOffset()+previousSegment.getLength());
	}
	
	/* clearTasks() [static]
	 * remove all the tasks in the tree
	 */
	public static void clearTasks(EvacuationTree tree) {
		for(EvacuationRoad r : tree.getRoads()) {
			for (Segment s : r.getRoad()) {
				s.clearTasks();
			}
		}
	}
	
//=================================================================//
//       * Getters *                                               //
//=================================================================//
	
	public int getSourceNode() {
		return sourceNode;
	}

	public int getFlow(int date) {
		if (date < offset) {
			return 0;
		} else if ((offset <= date)&&(date < offset+duration-1)) {
			return flow;
		} else if (date == offset+duration-1) {
			return residualFlow;
		} else {
			return 0;
		}
	}

	public int getFlow() {
		return flow;
	}

	public int getResidualFlow() {
		return residualFlow;
	}

	public int getDuration() {
		return duration;
	}

	public int getOffset() {
		return offset;
	}
	
}