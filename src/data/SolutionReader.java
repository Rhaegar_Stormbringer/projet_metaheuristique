package data;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SolutionReader {
	
	private File source;
	private File destination;
	
	private List<String> sourceContenu = new ArrayList<String>();
	private List<String> destinationContenu = new ArrayList<String>();
	
	public SolutionReader(String pathSource, String pathDestination) {
		
		// creation fichier source
		this.source = new File(pathSource);
		// creation fichier destination
		this.destination = new File(pathDestination);
		// creation buffer de lecture
		BufferedReader reader = null;
		String buffer = null;
		
		if (this.source.exists()) {
			// recuperation contenu fichier source
			try {
				reader = new BufferedReader(new FileReader(this.source));
				while ((buffer = reader.readLine()) != null) {
			        sourceContenu.add(buffer);
			    }
				reader.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			// remplissage contenu fichier destination
			buffer = patternMatch(pathSource, ".*/(.+)\\.full");
			if (buffer == null) {
				System.err.println("Erreur: nom de fichier invalide");
				destinationContenu.add("ERREUR[nom_fichier]");
				//System.exit(-1);
			} else {
				System.out.println("nom -> "+buffer);
				destinationContenu.add(buffer);
			}
			
			// recuperation nombre de sommets a evacuer
			int i = 0;
			i = getNextContent(i);
			System.out.println("nb noeur -> "+sourceContenu.get(i));
			String[] s = sourceContenu.get(i).split(" ");
			// recuperation nombres de noeuds a evacuer
			destinationContenu.add(s[0]);
		}
	}
	
	/*
	 * Retourne le premier motif de source qui valide regex.
	 * Retourne null en cas d'échec.
	 */
	private String patternMatch(String source, String regex) {
		String returnValue = null;
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(source);
		if (matcher.find()) {
			returnValue = matcher.group(1);
		}
		return returnValue;
	}
	
	private int getNextContent(int i) {
		while (sourceContenu.get(i).substring(0, 1).equals("c")) {
			i += 1;
		}
		return i;
	}
}